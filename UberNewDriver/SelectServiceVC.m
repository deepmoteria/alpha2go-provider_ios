//
//  SelectServiceVC.m
//  TaxiNow Driver
//
//  Created by My Mac on 3/23/15.
//  Copyright (c) 2015 Deep Gami. All rights reserved.
//

#import "SelectServiceVC.h"
#import "SelectServiceCell.h"

@interface SelectServiceVC ()
{
    NSMutableArray *arr;
    NSMutableArray *arrType;
    NSMutableArray *arrPricet;
    
    NSMutableArray *arrWtype;
    NSMutableArray *arrWtypePrice;
}
@end

@implementation SelectServiceVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    [super setBackBarItem];
    //self.btnRegister.enabled=NO;
    arr=[[NSMutableArray alloc] initWithObjects:@"one",@"two",@"three", nil];
    arrWtype=[[NSMutableArray alloc]init];
    arrWtypePrice=[[NSMutableArray alloc]init];
    arrPricet=[[NSMutableArray alloc]init];
    NSLog(@"register details :%@",self.dictparam);
    NSLog(@"image  :%@",self.imgP);
    [APPDELEGATE hideLoadingView];
    [APPDELEGATE showLoadingWithTitle:NSLocalizedString(@"Please Wait", nil)];
    [self getType];
    
    NSLog(@"log :%@",arrType);
}
-(void)viewWillAppear:(BOOL)animated
{
    arrType=[[NSMutableArray alloc]init];
    [self.btnMenu setTitle:NSLocalizedString(@"Register", nil) forState:UIControlStateNormal];
    [self.btnMenu setTitle:NSLocalizedString(@"Register", nil) forState:UIControlStateHighlighted];
    [self.btnRegister setTitle:NSLocalizedString(@"Register", nil) forState:UIControlStateNormal];
    [self.btnRegister setTitle:NSLocalizedString(@"Register", nil) forState:UIControlStateHighlighted];
    self.lblSelectService.text = NSLocalizedString(@"SELECR_SERVICE", nil);
    [self getType];
    
}

-(void)getType
{
    AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:GET_METHOD];
    [afn getDataFromPath:FILE_WALKER_TYPE withParamData:nil withBlock:^(id response, NSError *error)
     {
         if (response)
         {
             [APPDELEGATE hideLoadingView];
             if([[response valueForKey:@"success"] intValue]==1)
             {
                 [[AppDelegate sharedAppDelegate]hideHUDLoadingView];
                 arrType=[response valueForKey:@"types"];
                 NSLog(@"arr :%@",arrType);
                 for(int i=0;i<arrType.count;i++)
                 {
                     [arrPricet addObject:@""];
                     [arrWtype addObject:@""];
                 }
                 [self.tblselectService reloadData];
                 NSLog(@"Check %@",response);
             }
             else
             {
                 NSString *str = [response valueForKey:@"error_code"];
                 if([str intValue] == 406)
                 {
                     [self performSegueWithIdentifier:SEGUE_UNWIND sender:nil];
                 }
             }
         }
         
     }];
}

-(void)registerProvider
{
    [APPDELEGATE showLoadingWithTitle:NSLocalizedString(@"LOADING", nil)];
    NSString *strForTypeid;
    
    for (int i=0; i<arrWtype.count; i++)
    {
        strForTypeid = [arrWtype objectAtIndex:i];
        if(!strForTypeid.length)
        {
            [arrWtype removeObject:strForTypeid];
            [arrPricet removeObject:strForTypeid];
        }
    }
    
    strForTypeid=[arrWtype objectAtIndex:0];
    
    for (int i=1; i<arrWtype.count; i++)
    {
        strForTypeid=[strForTypeid stringByAppendingString:[NSString stringWithFormat:@",%@",[arrWtype objectAtIndex:i]]];
    }
    
    NSString *strForTypePrice=[arrPricet objectAtIndex:0];
    
    for (int i=1; i<arrPricet.count; i++)
    {
        strForTypePrice=[strForTypePrice stringByAppendingString:[NSString stringWithFormat:@",%@",[arrPricet objectAtIndex:i]]];
    }
    
    [self.dictparam setValue:strForTypeid forKey:PARAM_WALKER_TYPE];
    [self.dictparam setValue:strForTypePrice forKey:PARAM_WALKER_PRICE];
    
    AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
    
    [afn getDataFromPath:FILE_REGISTER withParamDataImage:self.dictparam andImage:self.imgP withBlock:^(id response, NSError *error)
     {
         [APPDELEGATE hideLoadingView];
         if (response)
         {
             if([[response valueForKey:@"success"] intValue]==1)
             {
                 [APPDELEGATE showToastMessage:NSLocalizedString(@"REGISTER_SUCCESS", nil)];
                 NSLog(@"res :%@",response);
                 [self.navigationController popToRootViewControllerAnimated:YES];
             }
             else
             {
                 NSString *str = [response valueForKey:@"error_code"];
                 if([str intValue] == 406)
                 {
                     [self performSegueWithIdentifier:SEGUE_UNWIND sender:nil];
                 }
                 else
                 {
                     NSString *str = [response valueForKey:@"error"];
                     UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:NSLocalizedString(str, nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
                     [alert show];
                 }
             }
         }
         NSLog(@"REGISTER RESPONSE --> %@",response);
     }];
}

#pragma mark textfield

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    #define ACCEPTABLE_CHARACTERS @"1234567890"
    
    NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:ACCEPTABLE_CHARACTERS] invertedSet];
    
    NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
    
    return [string isEqualToString:filtered];
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    UITextField *txtf=(UITextField *)textField;
    NSIndexPath *path = [NSIndexPath indexPathForRow:txtf.tag inSection:0];
    NSLog(@"index  :%ld",(long)txtf.tag);
    NSLog(@"index  :%ld",(long)path.row);
    NSDictionary *dictType=[arrType objectAtIndex:path.row];
    NSString *strTypeId=[NSMutableString stringWithFormat:@"%@",[dictType valueForKey:@"id"]];
    [arrPricet replaceObjectAtIndex:path.row withObject:textField.text];
    [arrWtype replaceObjectAtIndex:path.row withObject:strTypeId];
    NSLog(@"log :%@",arrPricet);
    self.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    UITextField *txtf=(UITextField *)textField;
    NSIndexPath *path = [NSIndexPath indexPathForRow:txtf.tag inSection:0];
    SelectServiceCell *Cell=[self.tblselectService cellForRowAtIndexPath:path];
    
    if(textField.text.length>0)
    {
        NSString *firstLetter = [textField.text substringToIndex:1];
                             
        firstLetter = [firstLetter uppercaseString];
        if([firstLetter isEqualToString:@"0"])
        {
            [APPDELEGATE showToastMessage:NSLocalizedString(@"Base price should not be zero", nil) ];
            //self.btnRegister.enabled=NO;
            Cell.imgCheck.image=[UIImage imageNamed:@"un_checkbox"];
            Cell.txtprice.background=[UIImage imageNamed:@"bg_price_g"];
            Cell.txtprice.enabled=NO;
            Cell.txtdoller.hidden=YES;
            [arrPricet replaceObjectAtIndex:path.row withObject:@""];
            [arrWtype replaceObjectAtIndex:path.row withObject:@""];
            Cell.txtprice.text=@"";
            //self.btnRegister.enabled=NO;
            return NO;
        }
        else
        {
            //self.btnRegister.enabled=YES;
            return YES;
        }
    }
    else if ([textField.text isEqualToString:@""])
    {
        [APPDELEGATE showToastMessage:NSLocalizedString(@"Please enter price", nil)];
        //self.btnRegister.enabled=NO;
        Cell.imgCheck.image=[UIImage imageNamed:@"un_checkbox"];
        Cell.txtprice.background=[UIImage imageNamed:@"bg_price_g"];
        Cell.txtprice.enabled=NO;
        Cell.txtdoller.hidden=YES;
        [arrPricet replaceObjectAtIndex:path.row withObject:@""];
        [arrWtype replaceObjectAtIndex:path.row withObject:@""];
        Cell.txtprice.text=@"";
        return NO;
    }
    else
    {
        //self.btnRegister.enabled=YES;
        return YES;
    }
}

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    SelectServiceCell *cell=(SelectServiceCell *)[self.tblselectService dequeueReusableCellWithIdentifier:@"PriceCell"];
    
    CGPoint origin = textField.frame.origin;
    CGPoint point = [textField.superview convertPoint:origin toView:self.tblselectService];
    
    NSIndexPath * indexPath = [self.tblselectService indexPathForRowAtPoint:point];
    
    NSLog(@"indexPath = %ld",(long)indexPath.row);
    
    self.view.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y-(30*(int)indexPath.row), self.view.frame.size.width, self.view.frame.size.height);
    return YES;
    
}
#pragma mark tableview

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrType.count;
    
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *dict=[arrType objectAtIndex:indexPath.row];
    
    SelectServiceCell *cell=(SelectServiceCell *)[tableView dequeueReusableCellWithIdentifier:@"PriceCell"];
    cell.lbltitle.text=[dict valueForKey:@"name"];
    cell.txtprice.tag=indexPath.row;
    cell.txtprice.enabled=NO;
    cell.txtdoller.hidden=YES;
    return cell;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 40.0f;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    SelectServiceCell *Cell=(SelectServiceCell *)[self.tblselectService cellForRowAtIndexPath:indexPath];
    if([Cell.imgCheck.image isEqual:[UIImage imageNamed:@"un_checkbox"]])
    {
        Cell.imgCheck.image=[UIImage imageNamed:@"check_box"];
        Cell.txtprice.background=[UIImage imageNamed:@"bg_price"];
        Cell.txtprice.enabled=YES;
        [Cell.txtprice becomeFirstResponder];
        NSDictionary *dictType=[arrType objectAtIndex:indexPath.row];
        NSString *strTypeId=[NSMutableString stringWithFormat:@"%@",[dictType valueForKey:@"id"]];
        NSString *StrPrice=[NSMutableString stringWithFormat:@"%@",[dictType valueForKey:@"base_price"]];
        Cell.txtprice.text = StrPrice;
        Cell.txtdoller.hidden=NO;
        NSLog(@"price = %@",arrPricet);
        NSLog(@"types = %@",arrWtype);
    }
    else
    {
        Cell.imgCheck.image=[UIImage imageNamed:@"un_checkbox"];
        Cell.imgs.image=[UIImage imageNamed:@"bg_price_g"];
        Cell.txtprice.enabled=NO;
        Cell.txtdoller.hidden=YES;
        
        Cell.txtprice.background=[UIImage imageNamed:@"bg_price_g"];
        NSDictionary *dictType=[arrType objectAtIndex:indexPath.row];
        NSString *strTypeId=[dictType valueForKey:@"id"];
        NSString *StrPrice=[dictType valueForKey:@"base_price"];
        NSLog(@"dict deselect  :%@",strTypeId);
        [arrPricet removeObject:StrPrice];
        [arrWtype removeObject:strTypeId];
        Cell.txtprice.text=@"";
        
        NSLog(@"price = %@",arrPricet);
        NSLog(@"types = %@",arrWtype);
    }
    [self.tblselectService deselectRowAtIndexPath:indexPath animated:YES];
}

-(void)btnCommentClick:(id)sender
{
    UIButton *senderButton = (UIButton *)sender;
    NSIndexPath *path = [NSIndexPath indexPathForRow:senderButton.tag inSection:0];
    SelectServiceCell *cell=[self.tblselectService cellForRowAtIndexPath:path];
    
    
    NSLog(@"text :%@",cell.lbltitle.text);
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)onClickforRegister:(id)sender
{
    [self.view endEditing:YES];
    int value=0;
    int value1=0;
    
    NSLog(@"price = %@",arrPricet);
    NSLog(@"types = %@",arrWtype);
    
    for(int i=0;i<arrWtype.count;i++)
    {
        NSString *str = [NSString stringWithFormat:@"%@",[arrWtype objectAtIndex:i]];
        if([str isEqualToString:@""])
        {
            value++;
        }
    }
    
    for(int i=0;i<arrPricet.count;i++)
    {
        NSString *str = [NSString stringWithFormat:@"%@",[arrPricet objectAtIndex:i]];
        if([str isEqualToString:@""])
        {
            value1++;
        }
    }
    
    if(value==arrWtype.count)
    {
        [APPDELEGATE showToastMessage:NSLocalizedString(@"Please select service.", nil)];
    }
    else if (value!=arrWtype.count)
    {
        if(value1==arrPricet.count)
        {
            [APPDELEGATE showToastMessage:NSLocalizedString(@"Please enter price", nil)];
        }
        else
        {
            [self registerProvider];
        }
    }
    else
    {
        [self registerProvider];
    }
}
@end
